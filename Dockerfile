FROM tomcat:8.0
COPY rewrite.config /usr/local/tomcat/conf/Catalina/localhost/
COPY server.xml /usr/local/tomcat/conf/
RUN echo "org.apache.catalina.core.ContainerBase.[Catalina].[localhost].level = FINE" >> conf/logging.properties

