
A quick demo to play with the Tomcat RewriteValve

```
#terminal A
docker build . -t tomcat-rewrite
docker run -p 8080:8080 tomcat-rewrite
```

```
#terminal B
curl -D- localhost:8080/foo
#shows
#HTTP/1.1 302 Found
#Location: /bar
```

Rewrite debug logs will show up in logs/localhost.2020-04-20.log (or whatever today is)

```
#get docker container instnace id
docker ps | grep tomcat-rewrite
#1b6600569ab3        tomcat-rewrite           "/bin/bash"         25 seconds ago      Up 24 seconds       0.0.0.0:8080->8080/tcp   compassionate_fermi

docker run lb660 cat logs/localhost.2020-04-20.log
```
